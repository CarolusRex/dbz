require 'test_helper'

class SubconceptsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subconcept = subconcepts(:one)
  end

  test "should get index" do
    get subconcepts_url
    assert_response :success
  end

  test "should get new" do
    get new_subconcept_url
    assert_response :success
  end

  test "should create subconcept" do
    assert_difference('Subconcept.count') do
      post subconcepts_url, params: { subconcept: { concept_id: @subconcept.concept_id, name: @subconcept.name } }
    end

    assert_redirected_to subconcept_url(Subconcept.last)
  end

  test "should show subconcept" do
    get subconcept_url(@subconcept)
    assert_response :success
  end

  test "should get edit" do
    get edit_subconcept_url(@subconcept)
    assert_response :success
  end

  test "should update subconcept" do
    patch subconcept_url(@subconcept), params: { subconcept: { concept_id: @subconcept.concept_id, name: @subconcept.name } }
    assert_redirected_to subconcept_url(@subconcept)
  end

  test "should destroy subconcept" do
    assert_difference('Subconcept.count', -1) do
      delete subconcept_url(@subconcept)
    end

    assert_redirected_to subconcepts_url
  end
end
