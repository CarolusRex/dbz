require 'test_helper'

class AdministrativeExpensesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @administrative_expense = administrative_expenses(:one)
  end

  test "should get index" do
    get administrative_expenses_url
    assert_response :success
  end

  test "should get new" do
    get new_administrative_expense_url
    assert_response :success
  end

  test "should create administrative_expense" do
    assert_difference('AdministrativeExpense.count') do
      post administrative_expenses_url, params: { administrative_expense: { card: @administrative_expense.card, concept_id: @administrative_expense.concept_id, day: @administrative_expense.day, mount: @administrative_expense.mount, payment_type: @administrative_expense.payment_type, pdf: @administrative_expense.pdf, period_id: @administrative_expense.period_id, ticket: @administrative_expense.ticket, user_id: @administrative_expense.user_id, xml: @administrative_expense.xml } }
    end

    assert_redirected_to administrative_expense_url(AdministrativeExpense.last)
  end

  test "should show administrative_expense" do
    get administrative_expense_url(@administrative_expense)
    assert_response :success
  end

  test "should get edit" do
    get edit_administrative_expense_url(@administrative_expense)
    assert_response :success
  end

  test "should update administrative_expense" do
    patch administrative_expense_url(@administrative_expense), params: { administrative_expense: { card: @administrative_expense.card, concept_id: @administrative_expense.concept_id, day: @administrative_expense.day, mount: @administrative_expense.mount, payment_type: @administrative_expense.payment_type, pdf: @administrative_expense.pdf, period_id: @administrative_expense.period_id, ticket: @administrative_expense.ticket, user_id: @administrative_expense.user_id, xml: @administrative_expense.xml } }
    assert_redirected_to administrative_expense_url(@administrative_expense)
  end

  test "should destroy administrative_expense" do
    assert_difference('AdministrativeExpense.count', -1) do
      delete administrative_expense_url(@administrative_expense)
    end

    assert_redirected_to administrative_expenses_url
  end
end
