Rails.application.routes.draw do
  get 'users/index'
  post 'users/index', to: "users#index"
  get 'users/approve/:iduser', to: "users#approve", as: 'approve'
  post 'users/approve/:iduser', to: "users#approve"

  devise_for :users
  resources :subconcepts
  get 'hour/index/:pro,:per', to: "hour#index", as: 'index'
  get 'periods_auto/index'
  get 'expenses/new/:per', to: "expenses#new", as: 'new_expense'

  resources :cards
  resources :workers
  resources :administrative_expenses
  resources :projects_users
  resources :expenses
  resources :concepts
  resources :periods
  resources :clients
  resources :tasks
  resources :projects
  resources :companies
  
  get 'welcome/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  root 'welcome#index'
  post '/expenses/new/:per', to: "expenses#new"
  post '/hour/index/:pro,:per', to: "hour#index"
  post '/administrative_expenses/new', to: "administrative_expenses#new"
  post '/concepts', to: "concepts#index"


end
