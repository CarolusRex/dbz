class CreatePeriods < ActiveRecord::Migration[5.0]
  def change
    create_table :periods do |t|
      t.date :date_start
      t.date :date_end
      t.integer :year

      t.timestamps
    end
  end
end
