class AddFormapagoToExpense < ActiveRecord::Migration[5.0]
  def change
    add_column :expenses, :payment_type, :integer
    add_column :expenses, :card, :integer
  end
end
