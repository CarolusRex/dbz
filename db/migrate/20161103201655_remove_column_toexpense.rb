class RemoveColumnToexpense < ActiveRecord::Migration[5.0]
  def change
  	remove_foreign_key :administrative_expenses, :users
    remove_column :administrative_expenses, :user_id
    remove_foreign_key :expenses, :users
    remove_column :expenses, :user_id
    remove_foreign_key :tasks, :users
    remove_column :tasks, :user_id
    remove_foreign_key :projects_users, :users
    remove_column :projects_users, :user_id
  end
end
