class AddColumnToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :work_position, :string
    add_column :users, :email, :string
    add_column :users, :cost, :decimal
  end
end
