class CreateHoursProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :hours_projects do |t|
      t.references :worker, foreign_key: true
      t.references :period, foreign_key: true
      t.references :project, foreign_key: true
      t.date :day
      t.integer :hours

      t.timestamps
    end
  end
end
