class AddPhotoToWorkers < ActiveRecord::Migration[5.0]
  def change
  	add_attachment :workers, :photo
  end
end
