class AddColumnToUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :worker, foreign_key: true
    add_column :users, :roles, :integer
    add_column :users, :who_update, :integer
  end
end
