class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.references :company, foreign_key: true
      t.integer :status
      t.date :start_date
      t.date :due_date

      t.timestamps
    end
  end
end
