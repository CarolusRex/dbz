class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.string :name
      t.references :project, foreign_key: true
      t.integer :status
      t.references :user, foreign_key: true
      t.date :start_date
      t.date :due_date

      t.timestamps
    end
  end
end
