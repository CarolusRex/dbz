class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :address
      t.integer :number_ext
      t.string :city
      t.integer :postcode
      t.string :state
      t.string :country
      t.string :rfc

      t.timestamps
    end
  end
end
