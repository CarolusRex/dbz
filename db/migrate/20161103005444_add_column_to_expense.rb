class AddColumnToExpense < ActiveRecord::Migration[5.0]
  def change
    add_column :expenses, :day, :date
  end
end
