class AddColumnToConcept < ActiveRecord::Migration[5.0]
  def change
    add_column :concepts, :type_concept, :integer
  end
end
