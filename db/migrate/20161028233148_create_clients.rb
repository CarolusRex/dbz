class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :financial_name
      t.string :address
      t.integer :number_ext
      t.string :city
      t.string :state
      t.string :country
      t.string :rfc
      t.string :colony
      t.integer :zip
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
