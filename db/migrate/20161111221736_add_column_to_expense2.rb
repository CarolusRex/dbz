class AddColumnToExpense2 < ActiveRecord::Migration[5.0]
  def change
    add_column :expenses, :refundable, :string
    add_column :expenses, :description, :text
    add_column :expenses, :workers_in, :string
  end
end
