class AddColumnToAdministrativeExpense < ActiveRecord::Migration[5.0]
  def change
    add_reference :administrative_expenses, :worker, foreign_key: true
	add_reference :expenses, :worker, foreign_key: true
  	add_reference :tasks, :worker, foreign_key: true
  	add_reference :projects_users, :worker, foreign_key: true   
  end
end
