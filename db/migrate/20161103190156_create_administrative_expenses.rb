class CreateAdministrativeExpenses < ActiveRecord::Migration[5.0]
  def change
    create_table :administrative_expenses do |t|
      t.references :period, foreign_key: true
      t.references :concept, foreign_key: true
      t.date :day
      t.decimal :mount
      t.references :user, foreign_key: true
      t.string :xml
      t.string :pdf
      t.string :ticket
      t.integer :payment_type
      t.integer :card

      t.timestamps
    end
  end
end
