class CreateWorkers < ActiveRecord::Migration[5.0]
  def change
    create_table :workers do |t|
      t.string :name
      t.string :last_name
      t.string :mothers_last_name
      t.string :work_position
      t.string :email
      t.decimal :cost_hour
      t.string :picture
      t.integer :status

      t.timestamps
    end
  end
end
