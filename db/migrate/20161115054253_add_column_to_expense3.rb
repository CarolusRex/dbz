class AddColumnToExpense3 < ActiveRecord::Migration[5.0]
  def change
  	add_column :expenses, :subtotal, :decimal
  	add_column :expenses, :vat, :decimal
  	add_reference :expenses, :subconcept, foreign_key: true
  end
end
