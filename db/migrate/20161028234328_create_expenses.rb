class CreateExpenses < ActiveRecord::Migration[5.0]
  def change
    create_table :expenses do |t|
      t.references :period, foreign_key: true
      t.references :project, foreign_key: true
      t.references :concept, foreign_key: true
      t.decimal :mount
      t.references :user, foreign_key: true
      t.string :xml_file
      t.string :pdf_file
      t.string :ticket

      t.timestamps
    end
  end
end
