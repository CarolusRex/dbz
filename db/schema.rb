# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161124195852) do

  create_table "administrative_expenses", force: :cascade do |t|
    t.integer  "period_id"
    t.integer  "concept_id"
    t.date     "day"
    t.decimal  "mount"
    t.string   "xml"
    t.string   "pdf"
    t.string   "ticket"
    t.integer  "payment_type"
    t.integer  "card"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "worker_id"
    t.index ["concept_id"], name: "index_administrative_expenses_on_concept_id"
    t.index ["period_id"], name: "index_administrative_expenses_on_period_id"
    t.index ["worker_id"], name: "index_administrative_expenses_on_worker_id"
  end

  create_table "cards", force: :cascade do |t|
    t.integer  "number"
    t.integer  "worker_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["worker_id"], name: "index_cards_on_worker_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "financial_name"
    t.string   "address"
    t.integer  "number_ext"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "rfc"
    t.string   "colony"
    t.integer  "zip"
    t.integer  "company_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["company_id"], name: "index_clients_on_company_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.integer  "number_ext"
    t.string   "city"
    t.integer  "postcode"
    t.string   "state"
    t.string   "country"
    t.string   "rfc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "concepts", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "type_concept"
  end

  create_table "expenses", force: :cascade do |t|
    t.integer  "period_id"
    t.integer  "project_id"
    t.integer  "concept_id"
    t.decimal  "mount"
    t.string   "xml_file"
    t.string   "pdf_file"
    t.string   "ticket"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "payment_type"
    t.integer  "card"
    t.date     "day"
    t.integer  "worker_id"
    t.string   "refundable"
    t.text     "description"
    t.string   "workers_in"
    t.decimal  "subtotal"
    t.decimal  "vat"
    t.integer  "subconcept_id"
    t.index ["concept_id"], name: "index_expenses_on_concept_id"
    t.index ["period_id"], name: "index_expenses_on_period_id"
    t.index ["project_id"], name: "index_expenses_on_project_id"
    t.index ["subconcept_id"], name: "index_expenses_on_subconcept_id"
    t.index ["worker_id"], name: "index_expenses_on_worker_id"
  end

  create_table "hours_projects", force: :cascade do |t|
    t.integer  "worker_id"
    t.integer  "period_id"
    t.integer  "project_id"
    t.date     "day"
    t.integer  "hours"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["period_id"], name: "index_hours_projects_on_period_id"
    t.index ["project_id"], name: "index_hours_projects_on_project_id"
    t.index ["worker_id"], name: "index_hours_projects_on_worker_id"
  end

  create_table "periods", force: :cascade do |t|
    t.date     "date_start"
    t.date     "date_end"
    t.integer  "year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "company_id"
    t.integer  "status"
    t.date     "start_date"
    t.date     "due_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["company_id"], name: "index_projects_on_company_id"
  end

  create_table "projects_users", force: :cascade do |t|
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "worker_id"
    t.index ["project_id"], name: "index_projects_users_on_project_id"
    t.index ["worker_id"], name: "index_projects_users_on_worker_id"
  end

  create_table "subconcepts", force: :cascade do |t|
    t.string   "name"
    t.integer  "concept_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["concept_id"], name: "index_subconcepts_on_concept_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "name"
    t.integer  "project_id"
    t.integer  "status"
    t.date     "start_date"
    t.date     "due_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "worker_id"
    t.index ["project_id"], name: "index_tasks_on_project_id"
    t.index ["worker_id"], name: "index_tasks_on_worker_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "worker_id"
    t.boolean  "approved",               default: false, null: false
    t.integer  "roles"
    t.integer  "who_update"
    t.index ["approved"], name: "index_users_on_approved"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["worker_id"], name: "index_users_on_worker_id"
  end

  create_table "workers", force: :cascade do |t|
    t.string   "name"
    t.string   "last_name"
    t.string   "mothers_last_name"
    t.string   "work_position"
    t.string   "email"
    t.decimal  "cost_hour"
    t.string   "picture"
    t.integer  "status"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "user_id"
    t.index ["user_id"], name: "index_workers_on_user_id"
  end

end
