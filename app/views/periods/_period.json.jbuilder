json.extract! period, :id, :date_start, :date_end, :year, :created_at, :updated_at
json.url period_url(period, format: :json)