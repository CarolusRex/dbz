json.extract! task, :id, :name, :project_id, :status, :user_id, :start_date, :due_date, :created_at, :updated_at
json.url task_url(task, format: :json)