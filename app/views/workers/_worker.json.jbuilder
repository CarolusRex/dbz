json.extract! worker, :id, :name, :last_name, :mothers_last_name, :work_position, :email, :cost_hour, :picture, :status, :created_at, :updated_at
json.url worker_url(worker, format: :json)