json.extract! project, :id, :name, :description, :company_id, :status, :start_date, :due_date, :created_at, :updated_at
json.url project_url(project, format: :json)