json.extract! client, :id, :name, :financial_name, :address, :number_ext, :city, :state, :country, :rfc, :colony, :zip, :company_id, :created_at, :updated_at
json.url client_url(client, format: :json)