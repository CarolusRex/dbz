json.extract! expense, :id, :period_id, :project_id, :concept_id, :mount, :user_id, :xml_file, :pdf_file, :ticket, :created_at, :updated_at
json.url expense_url(expense, format: :json)