json.extract! company, :id, :name, :address, :number_ext, :city, :postcode, :state, :country, :rfc, :created_at, :updated_at
json.url company_url(company, format: :json)