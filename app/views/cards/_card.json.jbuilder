json.extract! card, :id, :number, :worker_id, :created_at, :updated_at
json.url card_url(card, format: :json)