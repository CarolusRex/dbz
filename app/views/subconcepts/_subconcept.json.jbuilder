json.extract! subconcept, :id, :name, :concept_id, :created_at, :updated_at
json.url subconcept_url(subconcept, format: :json)