class Worker < ApplicationRecord
	belongs_to :user, optional: true
	has_attached_file :photo, styles:{thumb:"800x600", mini:"500x300"}
	
	validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/

	def self.photos(idworker)
		pho = Worker.find(idworker)
		phot = pho.photo
	end

	def self.names(idworker)
		na = Worker.find(idworker)
		nam = na.name 
	end

end
