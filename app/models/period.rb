class Period < ApplicationRecord

	def self.periodo(per)
		perinf = Period.find(per)
		"#{perinf.date_start.strftime("%v")} "  " al "  " #{perinf.date_end.strftime("%v")}"
	end

	def periodosAnual
    	Period.order(:date_start).find_by(year: 2016)
	end
  	def periodoRango
	    fInicio = Date.strptime("#{date_start}", "%Y-%m-%d")
	    fIni = fInicio.to_formatted_s(:short)
	    fFinal = Date.strptime("#{date_end}", "%Y-%m-%d")
	    fFin = fFinal.to_formatted_s(:short)
	    "#{fIni} - #{fFin}"
  	end

    def self.periodosAnualCombo
	  	fInicio = Time.now - 2.month
	  	fFin 	= Time.now
	  	Period.where(["date_start >= '%s' and date_end <= '%s'", fInicio, fFin]).order(:date_start)
	end

end
