class Expense < ApplicationRecord
  belongs_to :period
  belongs_to :project
  belongs_to :concept
  belongs_to :worker

  enum payment_type: [:efectivo, :tarjeta]
  after_initialize :set_default_payment_type, :if => :new_record?

  
  def set_default_payment_type
    self.payment_type ||= :efectivo
  end

  def self.dia_mes(fecha)
  	dia = fecha.cwday
  	if dia == 1
  		day = "Lun"
  	elsif dia == 2
  		day = "Mar"
	  elsif dia == 3
  		day = "Mie"
	  elsif dia == 4
  		day = "Jue"
	  elsif dia == 5
  		day = "Vie"
	  elsif dia == 6
  		day = "Sab"
	  elsif dia == 7
  		day = "Dom"
  	end
  	return day	
  end

  def self.checked(x,y)
    if x == y
      return :checked
    end
  end

end
