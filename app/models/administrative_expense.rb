class AdministrativeExpense < ApplicationRecord
  belongs_to :period
  belongs_to :concept
  belongs_to :worker

  enum payment_type: [:efectivo, :tarjeta]
  after_initialize :set_default_payment_type, :if => :new_record?

  
  def set_default_payment_type
    self.payment_type ||= :efectivo
  end

  def self.dia_mes(fecha)
  	dia = fecha.cwday
  	if dia == 1
  		day = "Lunes"
  	elsif dia == 2
  		day = "Martes"
	  elsif dia == 3
  		day = "Miercoles"
	  elsif dia == 4
  		day = "Jueves"
	  elsif dia == 5
  		day = "Viernes"
	  elsif dia == 6
  		day = "Sabado"
	  elsif dia == 7
  		day = "Domingo"
  	end
  	return day	
  end

end
