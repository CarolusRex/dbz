class User < ApplicationRecord
	has_one :worker
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def active_for_authentication? 
    super && approved? 
  end 

  def inactive_message 
    if !approved? 
      :not_approved 
    else 
      super # Use whatever other message 
    end 
  end

  enum roles: [:user_default, :admin]
  after_initialize :set_default_roles, :if => :new_record?

  
  def set_default_roles
    self.roles ||= :user_default
  end

end
