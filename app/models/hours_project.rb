class HoursProject < ApplicationRecord
  belongs_to :worker
  belongs_to :period
  belongs_to :project


  def self.hours_day(idw,ppe,proy,day)
  	horareg = HoursProject.select(:id,:day,:hours).where("hours_projects.period_id"=>ppe,"hours_projects.project_id"=>proy,"hours_projects.worker_id"=>idw) 
    hl = horareg.length
    for i in 0..hl-1 do
    	fecha = horareg[i].day
    	if fecha.cwday==day
    		cons = HoursProject.find_by(period_id:ppe,project_id:proy,day:fecha,worker_id:idw)
    		res = cons.hours
    	end
    end
    return res		
  end

  def self.hours_total(proy,per)
    horatot = HoursProject.select(:id,:day,:hours).where("hours_projects.period_id"=>per,"hours_projects.project_id"=>proy) 
    total = horatot.sum(:hours)
  end

  def self.periods_total(proy)
    pertot = HoursProject.select(:period_id).distinct.where("hours_projects.project_id"=>proy)
    pertot.count(:period_id)
  end

  def self.hours_id(idw,ppe,proy,day)
    horareg = HoursProject.select(:id,:day,:hours).where("hours_projects.period_id"=>ppe,"hours_projects.project_id"=>proy,"hours_projects.worker_id"=>idw) 
    hl = horareg.length
    for i in 0..hl-1 do
      fecha = horareg[i].day
      if fecha.cwday==day
        cons = HoursProject.find_by(period_id:ppe,project_id:proy,day:fecha,worker_id:idw)
        res = cons.id
      end
    end
    return res    
  end
end
