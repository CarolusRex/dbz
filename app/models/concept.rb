class Concept < ApplicationRecord
	
	enum type_concept: [:indefinido, :viatico, :administrativo]
    after_initialize :set_default_type_concept, :if => :new_record?

  
    def set_default_type_concept
    	self.type_concept ||= :indefinido
    end

	def self.names(idc)
		na = Concept.find(idc)
		nam = na.name 
	end
end
