class ExpenseDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari
  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= ['expenses.mount']
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= ['expenses.mount']
  end

  private

  def data
    records.map do |record|
      [
        record.concept_id,
        record.mount,
        record.day,
        record.period_id
      ]
    end
  end

  def get_raw_records
    # insert query here
    Expense.all
  end

  # ==== Insert 'presenter'-like methods below if necessary
end
