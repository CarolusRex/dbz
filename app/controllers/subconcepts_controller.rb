class SubconceptsController < ApplicationController
  before_action :set_subconcept, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  before_action :accesscontroller

  def accesscontroller
    if current_user.roles =="user_default"
      respond_to do |format|
          format.html {redirect_to welcome_index_path,notice: '' }
          format.js
      end
    end
  end


  # GET /subconcepts
  # GET /subconcepts.json
  def index
    @subconcepts = Subconcept.all
  end

  # GET /subconcepts/1
  # GET /subconcepts/1.json
  def show
  end

  # GET /subconcepts/new
  def new
    @subconcept = Subconcept.new
  end

  # GET /subconcepts/1/edit
  def edit
  end

  # POST /subconcepts
  # POST /subconcepts.json
  def create
    @subconcept = Subconcept.new(subconcept_params)

    respond_to do |format|
      if @subconcept.save
        format.html { redirect_to concepts_path, notice: 'Subconcept was successfully created.' }
        format.json { render :show, status: :created, location: @subconcept }
      else
        format.html { render :new }
        format.json { render json: @subconcept.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subconcepts/1
  # PATCH/PUT /subconcepts/1.json
  def update
    respond_to do |format|
      if @subconcept.update(subconcept_params)
        format.html { redirect_to concepts_path, notice: 'Subconcept was successfully updated.' }
        format.json { render :show, status: :ok, location: @subconcept }
      else
        format.html { render :edit }
        format.json { render json: @subconcept.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subconcepts/1
  # DELETE /subconcepts/1.json
  def destroy
    @subconcept.destroy
    respond_to do |format|
      format.html { redirect_to subconcepts_url, notice: 'Subconcept was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subconcept
      @subconcept = Subconcept.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subconcept_params
      params.require(:subconcept).permit(:name, :concept_id)
    end
end
