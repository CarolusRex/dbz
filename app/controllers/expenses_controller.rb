class ExpensesController < ApplicationController
  before_action :set_expense, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  

  # GET /expenses
  # GET /expenses.json
  def index
    @expense = Expense.all
  end

  # GET /expenses/1
  # GET /expenses/1.json
  def show
  end

  # GET /expenses/new
  def new 
    @expense = Expense.new
    @concept = Concept.select(:id,:name).where("concepts.type_concept"=>1)

    @periodos = Period.periodosAnualCombo
    @pro = nil
    @wor = nil
    @per = params[:per]
    @pernext = @per.to_i + 1
    @perbefore = @per.to_i - 1 
    if params[:expense]
      @dia = params[:x]
      @pro = params[:expense][:project_id]
      @con = params[:expense][:concept_id]
      if @con == nil
        @con = params[:concept]
      end
      @wor = params[:worker_id]
      @mon = params[:Monto]
      @tip = params[:tipo_pago]
      @com = params[:commit]
      xml = params[:xml]
      pdf = params[:pdf]
      ticket = params[:ticket]
      card = params[:expense][:card]
      des = params[:Descripcion]
      rem = params[:Reembolsable] 
      subtot = params[:Subtotal]
      subcon = params[:expense][:subconcept_id]
      iva = params[:Impuesto]
    end
    @subconcepts = Subconcept.select(:id,:name).where("subconcepts.concept_id"=>@con)
    if @per
      period = Period.find(@per)
      diaini = period.date_start
      diafin = period.date_end
      @arrdia = []
      while diaini <=diafin
        @arrdia << diaini
        diaini+=1
      end
    end 
    if @pro 
      @resp1 = true 
      pw = HoursProject.select(:worker_id).distinct.where("hours_projects.project_id"=>@pro,"hours_projects.period_id"=>@per)
      @arrpw = []
      pw.each do |w|
        @arrpw << Worker.find(w.worker_id)
      end
    end
    if @tip
      @resp2 = true
      if @tip == 'tarjeta'
        @resp3 = true 
        @tarjetas = Card.select(:id,:number,:worker_id).where("cards.worker_id"=> @wor)

      end
    end 
    if @com == "1"
      if !rem
        rem = "no"
      end
      worin = ""
      ind = 0
      for i in 0..pw.length-1 do
        par = ":r"+i.to_s
        no = params[par.parameterize.underscore.to_sym]
        if ind == 0
          esp = ""
        else
          esp = ", "
        end
        if no
          worin = worin + esp + no.to_s
          ind += 1
        end
      end 
      expins = Expense.new(:period_id => @per,:project_id => @pro, :concept_id => @con,:worker_id => @wor, :mount => @mon, :payment_type => @tip,:card =>card, :day =>@dia,:xml_file =>xml, :pdf_file=>pdf, :ticket=>ticket, :refundable=>rem, :description=>des, :workers_in=>worin, :subtotal=>subtot, :vat=>iva, :subconcept_id=>subcon ) 
      expins.save
      respond_to do |format|
        format.html {redirect_to new_expense_path(@per),notice: 'Gasto Guardado' }
        format.js
      end
      byebug
    end 
     #byebug
  end

  # GET /expenses/1/edit
  def edit
  end

  # POST /expenses
  # POST /expenses.json
  def create 
    @expense = Expense.new(expense_params)

    respond_to do |format|
      if @expense.save
        format.html { redirect_to @expense, notice: 'Expense was successfully created.' }
        format.json { render :show, status: :created, location: @expense }
      else
        format.html { render :new }
        format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end 
  end

  # PATCH/PUT /expenses/1
  # PATCH/PUT /expenses/1.json
  def update
    respond_to do |format|
      if @expense.update(expense_params)
        format.html { redirect_to @expense, notice: 'Expense was successfully updated.' }
        format.json { render :show, status: :ok, location: @expense }
      else
        format.html { render :edit }
        format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /expenses/1
  # DELETE /expenses/1.json
  def destroy
    @expense.destroy
    respond_to do |format|
      format.html { redirect_to expenses_url, notice: 'Expense was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_expense
      @expense = Expense.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def expense_params
      params.require(:expense).permit(:period_id, :project_id, :concept_id, :mount, :worker_id, :xml_file, :pdf_file, :ticket, :card, :payment_type,:day)
    end
end
