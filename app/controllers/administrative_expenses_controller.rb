class AdministrativeExpensesController < ApplicationController
  before_action :set_administrative_expense, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  before_action :accesscontroller

  def accesscontroller
    if current_user.roles =="user_default"
      respond_to do |format|
          format.html {redirect_to welcome_index_path,notice: '' }
          format.js
      end
    end
  end

  # GET /administrative_expenses
  # GET /administrative_expenses.json
  def index
    @administrative_expenses = AdministrativeExpense.all
  end

  # GET /administrative_expenses/1
  # GET /administrative_expenses/1.json
  def show
  end

  # GET /administrative_expenses/new
  def new
    @administrative_expense = AdministrativeExpense.new
    @concept = Concept.select(:id,:name).where("concepts.type_concept"=>2)
    @per = nil
    @con = nil
    @wor = nil
    @mon = nil
    @tipable = false
    if params[:administrative_expense]
      @per = params[:administrative_expense][:period_id]
      @dia = params[:x]
      @con = params[:administrative_expense][:concept_id]
      @wor = params[:administrative_expense][:worker_id]
      useaux = params[:worker_id]
      if useaux != ""
        @wor = useaux
      end
      @mon = params[:Monto]
      @tip = params[:tipo_pago]
      tipaux = params[:tipo_pagoaux]
      if tipaux != ""
        @tip = tipaux
      end
      @com = params[:commit]
      tar1 = params[:Tarjeta1]
      tar2 = params[:Tarjeta2]
      tar3 = params[:Tarjeta3]
      tar4 = params[:Tarjeta4]
      xml = params[:xml]
      pdf = params[:pdf]
      ticket = params[:ticket]
    end
    if @wor
      @sel = true
      if @wor == '2'
        @seladm =true
        if @tip == "tarjeta"
          @selemp = true
        elsif @tip ==  "efectivo"
          @selefe = true
          @tipable = true
        end
      else
        @selemp = true 
      end
    end
    if @com == '1'
      @nobutton = true 
    elsif @com == '2'
        @perable = true
    end
    if @com == 'Save changes'
      if @tip == 'tarjeta'
        tar = tar1+tar2+tar3+tar4
      end
      ins = AdministrativeExpense.new(period_id: @per, concept_id: @con, mount: @mon, worker_id: @wor, xml: xml, pdf: pdf, ticket: ticket, card: tar, payment_type: @tip,day: @dia)
      ins.save
      idcon = AdministrativeExpense.last.id
      respond_to do |format| 
        format.html { redirect_to administrative_expense_path(idcon), notice: 'Gasto Capturado y guardado.' }  
      end
    end
    if @per
      period = Period.find(@per)
      diaini = period.date_start
      diafin = period.date_end
      @arrdia = []
      while diaini <=diafin
        @arrdia << diaini
        diaini+=1
      end
    end

  end

  # GET /administrative_expenses/1/edit
  def edit
  end

  # POST /administrative_expenses
  # POST /administrative_expenses.json
  def create
    @administrative_expense = AdministrativeExpense.new(administrative_expense_params)

    respond_to do |format|
      if @administrative_expense.save
        format.html { redirect_to @administrative_expense, notice: 'Administrative expense was successfully created.' }
        format.json { render :show, status: :created, location: @administrative_expense }
      else
        format.html { render :new }
        format.json { render json: @administrative_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /administrative_expenses/1
  # PATCH/PUT /administrative_expenses/1.json
  def update
    respond_to do |format|
      if @administrative_expense.update(administrative_expense_params)
        format.html { redirect_to @administrative_expense, notice: 'Administrative expense was successfully updated.' }
        format.json { render :show, status: :ok, location: @administrative_expense }
      else
        format.html { render :edit }
        format.json { render json: @administrative_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /administrative_expenses/1
  # DELETE /administrative_expenses/1.json
  def destroy
    @administrative_expense.destroy
    respond_to do |format|
      format.html { redirect_to administrative_expenses_url, notice: 'Administrative expense was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_administrative_expense
      @administrative_expense = AdministrativeExpense.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def administrative_expense_params
      params.require(:administrative_expense).permit(:period_id, :concept_id, :day, :mount, :worker_id, :xml, :pdf, :ticket, :payment_type, :card)
    end
end
