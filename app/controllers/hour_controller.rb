class HourController < ApplicationController
  before_filter :authenticate_user!
  before_action :accesscontroller

  def accesscontroller
    if current_user.roles =="user_default"
      respond_to do |format|
          format.html {redirect_to welcome_index_path,notice: '' }
          format.js
      end
    end
  end

  def index
    @per = params[:per]
    @pro = params[:pro]
    @exp = Expense.select(:day,:worker_id,:concept_id,:mount).where("expenses.project_id"=>@pro,"expenses.period_id"=>@per)
 	  @worperpro = HoursProject.select(:worker_id).distinct.where("hours_projects.project_id"=>@pro,"hours_projects.period_id"=> @per)
 	  @pernext = @per.to_i + 1
    @perbefore = @per.to_i - 1 
    #modal -----------------------------
 	  pw = ProjectsUser.select(:worker_id).where("projects_users.project_id"=>@pro)
    @arrpw = []
    pw.each do |w|
      @arrpw << Worker.find(w.worker_id)
    end
    if params[:commit] == "AE"
      wor = params[:worker_id]
      valida = HoursProject.find_by(period_id:@per, project_id: @pro, worker_id:wor)
      if !valida
        periodo = Period.find(@per)
        diaini = periodo.date_start
        diafin = periodo.date_end
        while diaini <= diafin
          if diaini.cwday == 6 or diaini.cwday == 7
            horas = 0
          else
            horas = 8
          end
          hor = HoursProject.new(:worker_id=>wor,:period_id=>@per,:project_id=>@pro,:day=>diaini,:hours=>horas)
          hor.save
          diaini += 1
        end
      else
        respond_to do |format|
          format.html {redirect_to index_path(@pro, @per),alert: Worker.names(wor)+' YA TIENE HORAS ASIGNADAS, SELECCIONA UNO USUARIO QUE NO TENGA HORAS ASIGNADAS ' }
          format.js
        end
      end
    end
    #end of modal ---------------------
    #byebug
    if params[:commit] == "Edit"
      @edit = true
    end
    if params[:commit] == "Update"
      consid = HoursProject.select(:id).where(period_id:@per,project_id:@pro)
      consid.each do |x|
        par = "r"+x.id.to_s
        horupd = params[par.parameterize.underscore.to_sym]
        if horupd
          HoursProject.update(x.id,:hours => horupd)
        end
      end
    end
  end
end
